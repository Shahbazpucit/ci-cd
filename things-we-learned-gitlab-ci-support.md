# X Zany Things Support Engineers learned about GitLab CI through tickets

## Deep Dive: Auto-cancel redundant pipelines

Docs: [Auto-cancel redundant pipelines](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines)

**What is it?**: A feature that can be set on a per-project basis that permits you to control whether **pending** or **running** pipelines  are _automatically_ **canceled** when a new pipeline runs -- on the same branch. 

**Why would I use it?** To save CI minutes and compute resources. Multiple pipelines running on the same pipeline may be redundant. If you are troubleshooting a configuration matter on a GitLab Runner, it may make sense to run redundant pipelines and observe the output. 

**Show me an example!**

  - **TODO** An annotated example with `interruptible` but let's also record a walkthrough around this

### Controlling the setting

> What if I want to turn [Auto-cancel redundant pipelines](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines) on for the project but off for specific jobs?

Learn about the [interruptible](https://docs.gitlab.com/ee/ci/yaml/#interruptible) keyword. 

By default, jobs have `interruptible: false` which means that they are uninterruptible and will not be interrupted when made redundant. **NOTE** `interruptible` only matters for jobs that are **running** and does _not_ apply to **pending** jobs. 

  - **TODO** Say why only running and not pending --> designed to protect one-way operations from being interrupted. Who cares if a pending job is cancelled? Nothing started. 
  - **TODO** Include a blurb that makes understanding the relationship between [Auto-cancel redundant pipelines](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines) and the [interruptible](https://docs.gitlab.com/ee/ci/yaml/#interruptible) keyword accessible? (Take a pass at explaining it to a relatively tech-savvy person who is not working in tech)

### Troubleshooting

> The skeleton for now. So far, these are all things I would collect via the GitLab Rails Console. 

  - Was this job in a pipeline that was auto-cancelled?
  - Was this pipeline auto-cancelled?
  - Which pipelines in this project were auto-cancelled?
  - What pipeline caused this job or pipeline to be auto-cancelled?

#### Was this job in a pipeline that was auto-cancelled?

Use the sample below but you'll need to find a pipeline by job ID. 

See https://docs.gitlab.com/ee/administration/troubleshooting/gitlab_rails_cheat_sheet.html#cancel-stuck-pending-pipelines for a similar start


#### Check whether a pipeline was auto-cancelled

You will need to know the `id` of the project (or replace `find_by_id` with some other approach for finding the project) as well as the `id` of the pipeline. 

Let's assume we are looking for a project with an `id` of `42` and a pipeline whose `id` is `1337`. 

```
p = Project.find_by_id(42)
pp p.auto_cancel_pending_pipelines?
pipeline = Ci::Pipeline.find_by_id(1337)
pp pipeline
pp pipeline.status,pipeline.ref,pipeline.auto_canceled_by_id
```

**TODO** Update the above to read the value of `pipeline.auto_canceled_by_id` and say "The pipeline with ID `1337` was auto-cancelled by ___." or "The pipeine with ID `1337` was not auto-cancelled. It ____."

**TODO** Warn if **Auto-cancel redundant pipelines** is not on in the UI. It may have been on the past so we still want to check but it would be good to know that it is no longer on. 

Use something like:

```
if p.auto_cancel_pending_pipelines? ; puts "I cancel pending pipelines!"; else if not p.auto_can
cel_pending_pipelines?; puts "I do not cancel pending pipelines." ; end
irb(main):024:0> end
```

## TODOs

  - [ ] Link MRs that adjusted the text in the product!
   	 See https://gitlab.com/gitlab-org/gitlab/-/merge_requests/63795, find the other. 
  - [ ] Look through tickets/forums for common or interesting Qs about this setting 
  - [ ] Record video with Michael where we walk through everything in the article
    - [ ] Observe what happens with the setting off
    - [ ] Enable the setting
    - [ ] Observe the change in behavior
    - [ ] Toggle `interruptible` and observe the change in behavior

## Questions

We [say](https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines):

> You can set pending or running pipelines to cancel automatically when a new pipeline runs on the same branch.

  - **Q**: Does this apply to a new pipeline running on the same tag?

## Resources
  - https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/ci/pipeline.rb
  - https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/pipeline/chain/cancel_pending_pipelines.rb

## When linting isn't enough

See https://gitlab.zendesk.com/agent/tickets/236382

The gist: with  the right combination of `include` and `rules`, you can make a pipeline that lints but doesn't work properly. We have caveats. This is an example. 

